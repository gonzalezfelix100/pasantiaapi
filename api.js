const express = require("express");
const app = express();

const usuarios = require("./usuarios.js"); //Contiene las funciones para el manejo de los usuarios
const azure = require("./azure.js"); //Contiene las funciones para el manejo de azure
const couchdb = require("./couchdb.js"); //Contiene las funciones para el manejo de couchDB

//Utilizado para subir los archivos
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload')
const cors = require('cors');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload())
app.use(cors());

//Muestra por consola lo que se llama y su respuesta
const morgan = require('morgan');
app.use(morgan('dev'));

//Post para comprimir en un .tar.gz y luego subirlo a azure
//Recibe en el body los archivos a comprimir en un .tar.gz
//Recibe como parametros el nombre del container, el periodo(dly,wk,mth) y el nombre del usuario que lo sube
app.post(`/SubirA/:nameC/:periodo/:user`, async function (req, res) {
  res.setTimeout(7200000); //tiempo de respuesta 120 minutos
  try{
    let container = req.params.nameC;
    let periodo = req.params.periodo;
    let user = req.params.user;
    let files = req.body.filesNames;
    let nombre = req.body.nombre;
    let name = await azure.comprimir(files, periodo, user, nombre);
    await azure.sleep(2000); //Espera 2sg, para asegurar que se creo el .tar.gz
    let size = azure.getFilesize('./usuarios/' + user + '/uploads/' + name);
    let sizeString = size.toString();

    //si el .gz pesa 0.0000Mb, no se comprimio el .tar
    if(size == 0.0000){ 
      res.status(404).send(sizeString)
    }else{
      await azure.subirBlob(container, name, user);
      res.status(200).send(name + ' - ' + sizeString + 'Mb');
    }

  }catch (error) {
    res.status(404).send("error")
  }
  
});

//Post para guardar en el servidor los archivos
//Recibe los archivos a guardar
//Recibe como parametro el nombre del usuario que lo sube
app.post(`/Subir/:user`, async function (req, res) {
  let user = req.params.user;
  try {
    if(!req.files) {

      res.send({
          status: false,
          message: 'No file uploaded'
      });

    } else {

      let file = req.files.file;
      await file.mv('./usuarios/' + user + '/uploads/' + file.name);
      await res.send({
          status: true,
          message: 'File is uploaded',
          data: {
              name: file.name,
              mimetype: file.mimetype,
              size: file.size
          }
      });

    }

  } catch (err) {
      res.status(404).send(err);
  }

});

//Get para obtener los nombres de los containers
app.get('/Containers', async function (req, res,) {
  try{
    let containers = await azure.containersN();
    res.send(containers);
  }catch (error){
    res.status(404).send("error")
  }
});

//post para crear un container
app.post('/ContainersCrear', async function (req, res,) {
  try{
    let containers = await azure.containersCrear(req.body.container);
    res.send(containers);
  }catch (error){
    res.status(404).send("error")
  }
});

//post para eliminar un container
app.post('/ContainersEliminar', async function (req, res,) {
  try{
    let containers = await azure.containersEliminar(req.body.container);
    usuarios.actualizarContainers(req.body.container);
    usuarios.actualizarDescargas(req.body.container);
    res.send(containers);
  }catch (error){
    res.status(404).send("error")
  }
});

//Get para obtener los nombres de los blobs alojados en un container
//Recibe como parametro el nombre del container del que se quiere obtener los blobs
app.get(`/Blobs:name`, async function (req, res) {
  try{
    let blobs = await azure.containersBlobs(req.params.name);
    res.send(blobs);
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para eliminar un blob de un container
//Recibe como parametros el nombre del container y el nombre del blob que se quiere elminar
app.post(`/Eliminar/:containerN`, async function (req, res) {
  try{
    await azure.eliminarBlob(req.params.containerN, req.body.Blob);
    res.send("OK");
  }catch (error){
    res.status(404).send("error")
  }
});

//Get para la descarga de un blob alojado en un container
//Recibe como parametro el nombre del container y el nombre del blob a descargar
app.get(`/BlobsD/:nameC/:nameB`, async function (req, res) {
  try{
    let blob = await azure.descargarBlobs(req.params.nameC, req.params.nameB);
    res.send(blob);
  }catch (error){
    res.status(404).send("error")
  }
});

//Get para la descarga del ultimo blob que fue alojado en un container
//Recibe como parametro el nombre del container y el nombre del blob a descargar
app.get(`/BlobDescargaUltimo/:nameC`, async function (req, res) {
  try{
    let blob = await azure.descargarUltimoBlob(req.params.nameC);
    res.send(blob);
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para logearse
//Recibe en el body el nombre de usuario y la contraseña
app.post(`/Login`, async function (req, res) {
  try{
    let band = usuarios.verificarUsuario(req.body.user, req.body.password);
    if(band == 0){
      res.status(200).send("ok");
    }else if(band == 1){
      res.status(201).send("ok");//Contraseña incorrrecta
    }else{
      res.status(202).send("ok");//El usuario no existe
    }
  }catch (error){
    res.status(404).send("error")
  }
});

//Get para obtener los usuarios
app.get(`/Usuarios`, async function (req, res,) {
  try{
    let users = await usuarios.getUsuarios();
    res.send(users);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Post para agregar un nuevo usuario
//Recibe en el body el nombre de usuario y la contraseña
app.post(`/Usuarios`, async function (req, res,) {
  try{
    let users = await usuarios.setUsuarios(req.body.user, req.body.password);
    res.send(users);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Post para eliminar un usuario existente
//Recibe en el body el nombre de usuario
app.post(`/UsuariosE`, async function (req, res,) {
  try{
    let users = await usuarios.eliminarUsuario(req.body.user);
    res.send(users);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Post para cambiar la contraseña de un usuario existente
//Recibe en el body el nombre de usuario y la nueva contraseña
app.post(`/UsuariosM`, async function (req, res,) {
  try{
    let users = await usuarios.modificarUsuario(req.body.user, req.body.password);
    res.send(users);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Get para obtener los containers que puede ver un usuario
//Recibe como parametros el nombre de usuario
app.get(`/ContainersUsers/:user`, async function (req, res,) {
  try{
    let containers = await usuarios.getContainersUsuario(req.params.user);
    res.send(containers);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Post para guardar que containers puede ver un usuario
//Recibe en el body los containers que puede ver el usuario
//Recibe como parametros el nombre de usuario
app.post(`/ContainersUsers/:user`, async function (req, res,) {
  try{
    await usuarios.setContainersUsuario(req.params.user, req.body.containers);
    res.status(200).send('OK');
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Get para obtener los servidores que tiene un usuario
//Recibe como parametros el nombre de usuario
app.get(`/ServidoresUsers/:user`, async function (req, res,) {
  try{
    let servidores = await usuarios.getServidoresUsuario(req.params.user);
    res.send(servidores);
  }catch (error){
    res.status(404).send("error")
  }
  
});

//Post para guardar los servidores de un usuario
//Recibe en el body los servidores del usuario
//Recibe como parametros el nombre de usuario
app.post(`/ServidoresUsers/:user`, async function (req, res,) {
  try{
    let servidores = await usuarios.setServidoresUsuario(req.params.user, req.body.servidores);
    res.send(servidores);
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para obtener los nombres de las bases de datos de un servidor couchDB
//Recibe en el body el servidor del que se quieren las bases de datos
app.post(`/CouchdbBDs`, async function (req, res,) {
  try{
    let db = await couchdb.listarBD(req.body.servidor);
    res.send(db);
  }catch (error){
    let db = [];
    res.send(db);
  }
});

//Post para crear una bases de datos en un servidor couchDB
//Recibe en el body el nombre de la base de datos a crear
app.post(`/CouchdbCrearBD`, async function (req, res,) {
  try{
    await couchdb.crearBD(req.body.servidor, req.body.BD);
    res.send('OK');
  }catch (error){
    res.status(404).send('error');
  }
});

//Post para crear un backup de una base de datos
//Recibe en el body el servidor y la base de datos
//Recibe como parametros el nombre de usuario
app.post(`/CouchdbBackup/:user`, async function (req, res,) {
  res.setTimeout(3600000); //tiempo de respuesta 60 minutos
  try{

    let couchbackup = require('@cloudant/couchbackup'); //Utilizado para crear y restaurar backups en couchDB
    let fs = require('fs'); //Utilizado para trabajar con archivos y directorios
    let link = 'http://'+req.body.servidor[0].user+':'+req.body.servidor[0].password+'@'+req.body.servidor[0].ip+':'+req.body.servidor[0].port+'/'+req.body.BD;
    
    couchbackup.backup(
      link,
      fs.createWriteStream('./usuarios/' + req.params.user + '/backups/' + req.body.BD +'.txt'),
      {parallelism: 5, requestTimeout: 3600000},
      function(err) {
        if (err) {
          console.error(err);
        }
    }).on('finished', function () {
      res.status(200).send("OK")
    });

  }catch{
    res.status(404).send("error")
  }

});

//Get para descargar el backup creado
//Recibe como parametros el nombre de usuario y el nombre de la base de datos
app.get('/Download/:user/:BD', function(req, res){
  try{
    let file = './usuarios/'+ req.params.user +'/backups/'+ req.params.BD +'.txt';
    res.download(file, req.params.BD + '.txt');
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para restaurar un backup en una base de datos
//Recibe en el body el servidor y la base de datos
//Recibe como parametros el nombre de usuario
app.post(`/CouchdbRestore/:user`, async function (req, res,) {
  res.setTimeout(3600000); //tiempo de respuesta 60 minutos
  try{

    let couchbackup = require('@cloudant/couchbackup'); //Utilizado para crear y restaurar backups en couchDB
    let fs = require('fs'); //Utilizado para trabajar con archivos y directorios
    let link = 'http://'+req.body.servidor[0].user+':'+req.body.servidor[0].password+'@'+req.body.servidor[0].ip+':'+req.body.servidor[0].port+'/'+req.body.BD;
    
    couchbackup.restore(
      fs.createReadStream('./usuarios/' + req.params.user + '/backups/' + req.body.name),
      link,
      {requestTimeout: 3600000},
      function(err) {
        if (err) {
          console.error(err);
        }
    }).on('finished', function () {
      res.status(200).send("OK")
    });

  }catch{
    res.status(404).send("error")
  }

});

//Post para guardar en el servidor el backup
//Recibe el backup a guardar
//Recibe como parametro el nombre del usuario que lo sube
app.post(`/SubirBackup/:user`, async function (req, res) {
  let user = req.params.user;
  try {
    if(!req.files) {

      res.send({
          status: false,
          message: 'No file uploaded'
      });

    } else {

      let file = req.files.file;
      await file.mv('./usuarios/' + user + '/backups/' + file.name);
      await res.send({
          status: true,
          message: 'File is uploaded',
          data: {
              name: file.name,
              mimetype: file.mimetype,
              size: file.size
          }
      });

    }

  } catch (error) {
      res.status(500).send(error);
  }

});

//Post para actualizar las descargas programadas de un usuario
//Recibe en el body las descargas programadas
//Recibe como parametros el nombre de usuario
app.post(`/DescargasUsers/:user`, async function (req, res,) {
  try{
    let descargas = await usuarios.setDescargasUsuario(req.params.user, req.body.descargaP);
    res.send(descargas);
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para obtener las descargas programadas de un usuario
//Recibe como parametros el nombre de usuario
app.get(`/DescargasUsers/:user`, async function (req, res,) {
  try{
    let descargas = await usuarios.getDescargasUsuario(req.params.user);
    res.send(descargas);
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para obtener las credenciales de azure
//Recibe en el body el nombre de usuario
app.post(`/Credenciales`, async function (req, res,) {
  try{
    if(req.body.user.localeCompare("admin") == 0){
      let credenciales = await azure.getCredenciales();
      res.send(credenciales);
    }else{
      res.status(404)
    }
  }catch (error){
    res.status(404).send("error")
  }
});

//Post para modificar las credenciales de azure
//Recibe en el body las nuevas credenciales
app.post(`/Setcredenciales`, async function (req, res,) {
  try{
    await azure.setCredenciales(req.body.credenciales);
    usuarios.vaciarContainers();
    res.send('OK');
  }catch (error){
    res.status(404).send("error")
  }
});

//El puerto del servidor
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});