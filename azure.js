const {
    Aborter,
    BlockBlobURL,
    ContainerURL,
    ServiceURL,
    StorageURL,
    SharedKeyCredential,
    uploadFileToBlockBlob
} = require("@azure/storage-blob"); //Azure storage v10
const storage = require('azure-storage'); //Azure storage v2

//Utilizado para leer y escribir archivos
const fs = require('fs') //Utilizado para trabajar con archivos y directorios
const path = require("path"); //Utilizado para resolver paths

//Funcion que devuelve serviceURL para comunicarse con azure
//En esta funcion se utiliza azure storage V10
function credenciales() {
  let rawdata = fs.readFileSync('credenciales.json');
  let credenciales = JSON.parse(rawdata);
  let account = credenciales[0].cuenta;//Usuario de azure
  let accountKey = credenciales[0].contraseña;//contraseña de azure

  //Crear el service url, utilizado para comunicarse con azure
  let sharedKeyCredential = new SharedKeyCredential(account, accountKey);
  let pipeline = StorageURL.newPipeline(sharedKeyCredential);
  let serviceURL = new ServiceURL(
    `https://${account}.blob.core.windows.net`,
    pipeline
  );
  return serviceURL;
}

//Funcion que devuelve las credenciales de azure (cuenta y contraseña)
async function getCredenciales() {
  let rawdata = fs.readFileSync('credenciales.json');
  let credenciales = JSON.parse(rawdata);
  return credenciales;
}
module.exports.getCredenciales = getCredenciales;

//Funcion que modifica las credenciales de azure (cuenta y contraseña)
async function setCredenciales(credenciales) {
  let data = JSON.stringify(credenciales, null, 2);
  fs.writeFileSync('credenciales.json', data);
  return;
}
module.exports.setCredenciales = setCredenciales;

//Funcion que devuelve los nombres de todos los container alojados en azure
//En esta funcion se utiliza azure storage V10
async function containersN() {
  let serviceURL = credenciales();
  let marker; //Auxiliar
  let nombres = []; //Array de los nombres de los containers

  do {
    let listContainersResponse = await serviceURL.listContainersSegment(
      Aborter.none,
      marker
    );

    marker = listContainersResponse.nextMarker;
    for (let container of listContainersResponse.containerItems) {
      nombres.push(container.name);
    }
  } while (marker);

  return nombres;
}
module.exports.containersN = containersN;

//Funcion que crea un container en azure
//En esta funcion se utiliza azure storage V10
async function containersCrear(containerName) {
  let serviceURL = credenciales();
  let containerURL = ContainerURL.fromServiceURL(serviceURL, containerName); //URL del container a crear
  await containerURL.create(Aborter.none); //Crear el container
  return containersN();
}
module.exports.containersCrear = containersCrear;

//Funcion que elimina un container de azure
//En esta funcion se utiliza azure storage V10
async function containersEliminar(containerName) {
  let serviceURL = credenciales();
  let containerURL = ContainerURL.fromServiceURL(serviceURL, containerName); //URL del container a eliminar
  await containerURL.delete(Aborter.none); //Eliminar el container
  return containersN();
}
module.exports.containersEliminar = containersEliminar;

//Funcion que devuelve los nombres de todos los blobs dentro de un container alojado en azure
//En esta funcion se utiliza azure storage V10
async function containersBlobs(containerName) {
  let serviceURL = credenciales();
  //URL del container a revisar
  let containerURL = ContainerURL.fromServiceURL(serviceURL, containerName);

  let marker; //Auxiliar
  let blobsN = []; //Array de los nombres de los blobs

  do {
    const listBlobsResponse = await containerURL.listBlobFlatSegment(
      Aborter.none,
      marker
    );

    marker = listBlobsResponse.nextMarker;
    for (const blob of listBlobsResponse.segment.blobItems) {
      blobsN.push(blob.name);
    }
  } while (marker);

  return blobsN;
}
module.exports.containersBlobs = containersBlobs;
  
//Funcion que devuelve un url para poder descargar un blob
//En esta funcion se utiliza azure storage V2
async function descargarBlobs(containerName, blobName) {
  let rawdata = fs.readFileSync('credenciales.json');
  let credenciales = JSON.parse(rawdata);
  let account = credenciales[0].cuenta;//Usuario de azure
  let accountKey = credenciales[0].contraseña;//contraseña de azure

  //Variables para la creacion del token
  let startDate = new Date();
  let expiryDate = new Date(startDate);
  expiryDate.setMinutes(startDate.getMinutes() + 100);
  startDate.setMinutes(startDate.getMinutes() - 100);

  //Utilizada para comunicarse con azure
  let blobService = storage.createBlobService(account, accountKey);

  //Token de autorizacion
  let sastoken = blobService.generateSharedAccessSignature(containerName, blobName, {AccessPolicy: {
    Permissions: storage.BlobUtilities.SharedAccessPermissions.READ,
    Start: startDate,
    Expiry: expiryDate
  }});

  //Regresa el url para descargar el blob
  return await blobService.getUrl(containerName, blobName, sastoken);
}
module.exports.descargarBlobs = descargarBlobs;

//Funcion que devuelve el ultimo blob creado en un container
//Busca hasta 2 años atras, y si no encuentra un blob devuelve un '0'
async function ultimo(containerName) {
  let blobs = await containersBlobs(containerName);
  let fecha = new Date;
  let mes = "";
  let año = fecha.getFullYear();
  let blobs1 = []; //Array auxiliar

  if(fecha.getMonth() < 10){
    mes= "0" + (fecha.getMonth() + 1);
  }else{
    mes= (fecha.getMonth() + 1);
  }

  //Obtener los blobs del mes mas reciente
  for(let i = 0; i < blobs.length; i++){
    let aux = "";
    aux = blobs[i];
    if(aux.indexOf("-") > -1){
      aux.slice(aux.indexOf("-")+3, aux.indexOf("-")+9)
      if(aux.includes(mes+año)){
        blobs1.push(blobs[i]);
      }
    }
    if( (i == (blobs.length-1)) && ((blobs1.length == 0))){
      i=0;
      if(Number(mes) == 1){
        mes = "12"
        año = (Number(año)-1).toString();
        if(Number(año) < (fecha.getFullYear()-2) ){
          return "0";
        }
      }else{
        if((Number(mes)-1) < 10){
          mes = "0" + (Number(mes)-1).toString();
        }else{
          mes = (Number(mes)-1).toString();
        }
      }
    }
  }
  blobs = blobs1;

  //Obtener el blob del dia y la hora mas reciente
  for(let i = 0; i < blobs.length; i++){
    let aux = "";
    let dia = 0;
    let pos = -1;
    aux = blobs[i];
    if(aux.indexOf("-") > -1){
      if(dia == 0){
        dia = Number(aux.slice(aux.indexOf("-")+1, aux.indexOf("-")+3))
        pos = i;
      }else if(dia < Number(aux.slice(aux.indexOf("-")+1, aux.indexOf("-")+3))){
        dia = Number(aux.slice(aux.indexOf("-")+1, aux.indexOf("-")+3))
        pos = i;
      }else if(dia == Number(aux.slice(aux.indexOf("-")+1, aux.indexOf("-")+3))){
        if(Number(blobs[pos].slice(blobs[pos].indexOf("-")+10, blobs[pos].indexOf("-")+16)) < Number(aux.slice(aux.indexOf("-")+10, aux.indexOf("-")+16))){
          dia = Number(aux.slice(aux.indexOf("-")+1, aux.indexOf("-")+3))
          pos = i;
        }
      }
    }
    if( i == (blobs.length-1) ){
      if(pos == -1){
        return "0";
      }else{
        return blobs[pos];
      }
    }
  }
  return "0";

}

//Funcion que devuelve un url para poder descargar el ultimo blob que fue alojado en un container
//En esta funcion se utiliza azure storage V2
async function descargarUltimoBlob(containerName) {
  let rawdata = fs.readFileSync('credenciales.json');
  let credenciales = JSON.parse(rawdata);
  let account = credenciales[0].cuenta;//Usuario de azure
  let accountKey = credenciales[0].contraseña;//contraseña de azure
  
  //Variables para la creacion del token
  let startDate = new Date();
  let expiryDate = new Date(startDate);
  expiryDate.setMinutes(startDate.getMinutes() + 100);
  startDate.setMinutes(startDate.getMinutes() - 100);

  //Utilizada para comunicarse con azure
  let blobService = storage.createBlobService(account, accountKey);

  //Nombre del blob a descargar
  let blobName = await ultimo(containerName);

  //Si el nombre del blob es '0', significa que no encontro un blob para descargar
  if(Number(blobName) == 0){
    return;
  }

  //Token de autorizacion
  let sastoken = blobService.generateSharedAccessSignature(containerName, blobName, {AccessPolicy: {
    Permissions: storage.BlobUtilities.SharedAccessPermissions.READ,
    Start: startDate,
    Expiry: expiryDate
  }});

  //Regresa el url para descargar el blob
  return await blobService.getUrl(containerName, blobName, sastoken);
}
module.exports.descargarUltimoBlob = descargarUltimoBlob;

//Funcion para comprimir los archivos en un .tar.gz
async function comprimir(fileN, periodo, user, nombre) {
    let fecha = new Date();
    let dia = "";
    let meses = fecha.getMonth() + 1;
    let mes = "";
    let hora = "";
    let min = "";
    let seg = "";
  
    if(fecha.getDate() < 10){
      dia= "0" + fecha.getDate();
    }else{
      dia= fecha.getDate();
    }
  
    if(meses < 10){
      mes= "0" + meses;
    }else{
      mes= meses;
    }
  
    if((fecha.getHours()) < 10){
      hora= "0" + fecha.getHours();
    }else{
      hora= fecha.getHours();
    }
  
    if(fecha.getMinutes() < 10){
      min= "0" + fecha.getMinutes();
    }else{
      min= fecha.getMinutes();
    }
  
    if(fecha.getSeconds() < 10){
      seg= "0" + fecha.getSeconds();
    }else{
      seg= fecha.getSeconds();
    }
    
    let name = nombre + "-" + dia + mes + fecha.getFullYear() +"-"+ hora + min + seg +"-pr-"+ periodo +".tar.gz";
  
    let zlib = require('zlib'); //Utilizado para comprimir en .gz
    let tar = require('tar-fs') //Utilizado para comprimir en .tar

    //Crear el comprimido
    tar.pack('./usuarios/' + user + '/uploads', {
      entries: fileN // only the specific entries will be packed
    }).pipe(zlib.createGzip())
    .on('error', console.log)
    .pipe(fs.createWriteStream('./usuarios/' + user + '/uploads/' + name))
    .on('error', console.log);;
  
    return name
}
module.exports.comprimir = comprimir;

//Funcion para esperar, utiliza ms, 1000ms = 1sg
async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
module.exports.sleep = sleep;
  
//Funcion que sube un blob a azure
//En esta funcion se utiliza azure storage V10
async function subirBlob(containerName, name, user) {
    let file = './usuarios/' + user + '/uploads/' + name;
    let filePath = path.resolve(file);
    let fileName = path.basename(filePath);
  
    let serviceURL = credenciales();
    let containerURL = ContainerURL.fromServiceURL(serviceURL, containerName);
    let blockBlobURL = BlockBlobURL.fromContainerURL(containerURL, fileName);
  
    return await uploadFileToBlockBlob(Aborter.none, filePath, blockBlobURL);
}
module.exports.subirBlob = subirBlob;
  
//Funcion que elimina un blob de un container de azure
//En esta funcion se utiliza azure storage V2
async function eliminarBlob(containerName, blobName) {
  let rawdata = fs.readFileSync('credenciales.json');
  let credenciales = JSON.parse(rawdata);
  let account = credenciales[0].cuenta;//Usuario de azure
  let accountKey = credenciales[0].contraseña;//contraseña de azure

  //Utilizada para comunicarse con azure
  let blobService = storage.createBlobService(account, accountKey);

  return await new Promise((resolve, reject) => {
    blobService.deleteBlobIfExists(containerName, blobName, err => {
      if (err) {
          reject(err);
      } else {
          resolve(true);
      }
    });
  });
}
module.exports.eliminarBlob = eliminarBlob;
  
//Funcion que devuelve el peso de un archivo en Mb
function getFilesize(filename) {
    let stats = fs.statSync(filename);
    let fileSizeInBytes = stats["size"];
    let fileSizeInMegabytes = fileSizeInBytes / 1000000.0;
    return fileSizeInMegabytes;
}
module.exports.getFilesize = getFilesize;