//Funcion que devuelve los nombres de las bases de datos de un servidor couchDB
async function listarBD(server) {
  //"nano" es utilizado para comunicarse con couchDB
  let nano = require('nano')('http://'+server[0].user+':'+server[0].password+'@'+server[0].ip+':'+server[0].port);
  return await nano.db.list();
}
module.exports.listarBD = listarBD;


//Funcion para crear una bases de datos en un servidor couchDB
async function crearBD(server, BD) {
  //"nano" es utilizado para comunicarse con couchDB
  let nano = require('nano')('http://'+server[0].user+':'+server[0].password+'@'+server[0].ip+':'+server[0].port);
  return await nano.db.create(BD);
}
module.exports.crearBD = crearBD;
