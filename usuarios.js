const path = require("path"); //Utilizado para resolver paths
const fs = require('fs'); //Utilizado para trabajar con archivos y directorios

//Funcion que comprueba que el usuario ingresado existe y que la contraseña ingresada es correcta
//Devuelve "0" si el usuario existe y la contraseña es correcta
//Devuelve "1" si el usuario existe y la contraseña es incorrecta
//Devuelve "2" si el usuario no existe
function verificarUsuario(usuario, contraseña) {
  let rawdata = fs.readFileSync('usuarios.json');
  let usuarios = JSON.parse(rawdata);
  for(n in usuarios){
    if(usuarios[n].user == usuario){
      if(usuarios[n].password == contraseña){
        return 0;
      }else{
        return 1;
      }
    }
  }
  return 2;
}
module.exports.verificarUsuario = verificarUsuario;

//Funcion para obtener los usuarios existentes
function getUsuarios() {
  let rawdata = fs.readFileSync('usuarios.json');
  let usuarios = JSON.parse(rawdata);
  return usuarios;
}
module.exports.getUsuarios = getUsuarios;

//Funcion que crea un nuevo usuario, con sus carpetas y archivos
function setUsuarios(usuario, contraseña) {
  let rawdata = fs.readFileSync('usuarios.json');
  let usuarios = JSON.parse(rawdata);

  usuarios.push({user: usuario, password: contraseña})
  let data = JSON.stringify(usuarios, null, 2);
  fs.writeFileSync('usuarios.json', data);

  let dir = './usuarios/' + usuario;
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
    fs.mkdirSync(dir + '/uploads');
    fs.mkdirSync(dir + '/backups');
    fs.writeFileSync(dir + '/containers.json', JSON.stringify([]));
    fs.writeFileSync(dir + '/servidores.json', JSON.stringify([]));
    fs.writeFileSync(dir + '/descargas.json', JSON.stringify([]));
  }else{
    fs.mkdirSync(dir + '/uploads');
    fs.writeFileSync(dir + '/containers.json', JSON.stringify([]));
    fs.writeFileSync(dir + '/servidores.json', JSON.stringify([]));
    fs.writeFileSync(dir + '/descargas.json', JSON.stringify([]));
  }

  return usuarios;
}
module.exports.setUsuarios = setUsuarios;

//Funcion elimina un usuario, con sus carpetas y archivos
function eliminarUsuario(usuario) {
    let rawdata = fs.readFileSync('usuarios.json');
    let usuarios = JSON.parse(rawdata);

    let aux = [];
    for(n in usuarios){
        if(!(usuarios[n].user.localeCompare(usuario) == 0)){
        aux.push(usuarios[n]);
        }
    }

    let data = JSON.stringify(aux, null, 2);
    fs.writeFileSync('usuarios.json', data);

    let dir = './usuarios/' + usuario + '/uploads/';
    let files = fs.readdirSync(dir);
    for (let file of files) {
      fs.unlinkSync(path.join(dir, file));
    };
    fs.rmdirSync(dir);

    dir = './usuarios/' + usuario + '/backups/';
    files = fs.readdirSync(dir);
    for (let file of files) {
      fs.unlinkSync(path.join(dir, file));
    };
    fs.rmdirSync(dir);

    dir = './usuarios/' + usuario;
    files = fs.readdirSync(dir);
    for (let file of files) {
      fs.unlinkSync(path.join(dir, file));
    };
    fs.rmdirSync(dir);

    return aux;
}
module.exports.eliminarUsuario = eliminarUsuario;

//Funcion que cambia la contraseña de un usuario
function modificarUsuario(usuario, contraseña) {
    let rawdata = fs.readFileSync('usuarios.json');
    let usuarios = JSON.parse(rawdata);

    for(n in usuarios){
        if((usuarios[n].user.localeCompare(usuario) == 0)){
        usuarios[n].password = contraseña;
        break;
        }
    }

    return usuarios;
}
module.exports.modificarUsuario = modificarUsuario;

//Funcion que obtiene los containers que puede ver un usuario
function getContainersUsuario(usuario) {
    let rawdata = fs.readFileSync('./usuarios/' + usuario + '/containers.json');
    let containers = JSON.parse(rawdata);
    return containers;
}
module.exports.getContainersUsuario = getContainersUsuario;

//Funcion que actualiza los containers que puede ver un usuario
function setContainersUsuario(usuario, containersU) {
    let data = JSON.stringify(containersU, null, 2);
    fs.writeFileSync('./usuarios/' + usuario + '/containers.json', data);
    return;
}
module.exports.setContainersUsuario = setContainersUsuario;

//Funcion que actualiza los containers que pueden ver los usuarios
function actualizarContainers(container) {
  let users = getUsuarios();

  for(let i = 1; i < users.length; i++){
    let containers = getContainersUsuario(users[i].user);
    let aux = [];
    for(let j = 0; j < containers.length; j++){
      if(container.localeCompare(containers[j]) != 0){
        aux.push(containers[j]);
      }
    }
    let data = JSON.stringify(aux, null, 2);
    fs.writeFileSync('./usuarios/' + users[i].user + '/containers.json', data);
  }
  return;
}
module.exports.actualizarContainers = actualizarContainers;

//Funcion que resetea los containers que pueden ver los usuarios
function vaciarContainers() {
  let users = getUsuarios();
  for(let i = 1; i < users.length; i++){
    let aux = [];
    let data = JSON.stringify(aux, null, 2);
    fs.writeFileSync('./usuarios/' + users[i].user + '/containers.json', data);
  }
  return;
}
module.exports.vaciarContainers = vaciarContainers;

//Funcion que obtiene los servidores guardados de un usuario
function getServidoresUsuario(usuario) {
    let rawdata = fs.readFileSync('./usuarios/' + usuario + '/servidores.json');
    let servidores = JSON.parse(rawdata);
    return servidores;
}
module.exports.getServidoresUsuario = getServidoresUsuario;

//Funcion que actualiza los servidores guardados de un usuario
function setServidoresUsuario(usuario, servidoresU) {
    let data = JSON.stringify(servidoresU, null, 2);
    fs.writeFileSync('./usuarios/' + usuario + '/servidores.json', data);
    let rawdata = fs.readFileSync('./usuarios/' + usuario + '/servidores.json');
    let servidores = JSON.parse(rawdata);
    return servidores;
}
module.exports.setServidoresUsuario = setServidoresUsuario;

//Funcion que obtiene las descargas programadas de un usuario
function getDescargasUsuario(usuario) {
  let rawdata = fs.readFileSync('./usuarios/' + usuario + '/descargas.json');
  let descargas = JSON.parse(rawdata);
  return descargas;
}
module.exports.getDescargasUsuario = getDescargasUsuario;

//Funcion que actualiza las descargas programadas de un usuario
function setDescargasUsuario(usuario, descargasP) {
  let data = JSON.stringify(descargasP, null, 2);
  fs.writeFileSync('./usuarios/' + usuario + '/descargas.json', data);
  let rawdata = fs.readFileSync('./usuarios/' + usuario + '/descargas.json');
  let descargas = JSON.parse(rawdata);
  return descargas;
}
module.exports.setDescargasUsuario = setDescargasUsuario;

//Funcion que actualiza los containers que pueden ver los usuarios
function actualizarDescargas(container) {
  let users = getUsuarios();

  for(let i = 0; i < users.length; i++){
    let descargas = getDescargasUsuario(users[i].user);
    let aux = [];
    for(let j = 0; j < descargas.length; j++){
      if(container.localeCompare(descargas[j].container) != 0){
        aux.push(descargas[j]);
      }
    }
    let data = JSON.stringify(aux, null, 2);
    fs.writeFileSync('./usuarios/' + users[i].user + '/descargas.json', data);
  }
  return;
}
module.exports.actualizarDescargas = actualizarDescargas;